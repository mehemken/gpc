# **G**eneral **P**urpose **C**onversion

This is a simple command line tool I created as an answer to *Exercise 2.2* in "The Go Programming Language" by Alan Donovan and Brian Kernigan. I highly recommend this book to anyone learning Go.

The prompt is as follows.

**Exercise 2.2:** *Write a general-purpose unit-conversion program analagous to `cf` that reads numbers from its command-line arguments or from the standard input if there are no arguments, and convers each number into units like temperature in Celsius and Fahrenheit, length in feet and meters, weight in punds and kilograms, and the like.*

I welcome any and all critiques and contributions.

# Usage examples

```
$ gpc -ctof 3  # 3°C to °F
37.4°F

$ gpc -intoyd 403
11.194444444444445yd

$ gpc -intoft 83
6'11.0"

$ echo 88 | gpc -mmtom
0.088m

$ python -c 'print(80+8)' | gpc -mmtocm
8.8cm
```

As you can see it is not perfect. There are a few details to iron out. Yards (and other units) display with unnecessary precision. There are also very many units missing from this project. I have not added any mass or volume units and I haven't even written conversion functions for metric to/from imperial distances.
