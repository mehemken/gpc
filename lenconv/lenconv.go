// lenconv converts from feet to meters
package lenconv

import (
    "fmt"
    "math"
)

//####################
// Types - metric
//####################

type Meter float64
type Centimeter float64
type Milimeter float64
type Kilometer float64

func (m Meter) String() string { return fmt.Sprintf("%gm", m) }
func (m Centimeter) String() string { return fmt.Sprintf("%gcm", m) }
func (m Milimeter) String() string { return fmt.Sprintf("%gmm", m) }
func (m Kilometer) String() string { return fmt.Sprintf("%gkm", m) }

//####################
// Conversions - metric
//####################

func MToCm(m Meter) Centimeter { return Centimeter(m*100) }
func MToMm(m Meter) Milimeter { return Milimeter(m*1000) }
func MToKm(m Meter) Kilometer { return Kilometer(m/1000) }

func CmToM(cm Centimeter) Meter { return Meter(cm*100) }
func CmToMm(cm Centimeter) Milimeter { return Milimeter(cm*10) }
func CmToKm(cm Centimeter) Kilometer { return Kilometer(cm*100000) }

func MmToM(mm Milimeter) Meter { return Meter(mm/1000) }
func MmToCm(mm Milimeter) Centimeter { return Centimeter(mm/10) }
func MmToKm(mm Milimeter) Kilometer { return Kilometer(mm/1000000) }

//####################
// Types - imperial
//####################

type Foot float64
type Inch float64
type Yard float64
// type Mile float64

func (f Foot) String() string {
    floorf := math.Floor(float64(f))
    return fmt.Sprintf("%g'%.1f\"", floorf, (float64(f) - floorf)*12)
}
func (i Inch) String() string { return fmt.Sprintf("%gin", i) }
func (y Yard) String() string { return fmt.Sprintf("%gyd", y) }

//####################
// Conversions - imperial
//####################

func FtToIn(ft Foot) Inch { return Inch(ft*12) }
func FtToYd(ft Foot) Yard { return Yard(ft/3) }

func InToFt(in Inch) Foot { return Foot(in/12) }
func InToYd(in Inch) Yard {
    ft := InToFt(in)
    return FtToYd(ft)
}

func YdToFt(yd Yard) Foot { return Foot(yd*3) }
func YdToIn(yd Yard) Inch {
    ft := YdToFt(yd)
    return FtToIn(ft)
}

