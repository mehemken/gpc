package main

import (
    "fmt"
    "os"
    "strconv"
    "gopl/gpc/tempconv"
    "gopl/gpc/lenconv"
    "bufio"
    "flag"
    "errors"
)

//########################################
// Types
//########################################

type conversion struct {
    // 0 - Success
    // 1 - Fail
    Status int
    Value string
}

func (c conversion) toFloat() (float64, error) {
    f, err := strconv.ParseFloat(c.Value, 64)
    if err != nil {
	return 0.0, err
    }
    return f, nil
}

//########################################
// Main
//########################################

func main() {
    // Temperature conversions
    flagCToF := flag.Bool("ctof", false, "Celsius to Fahrenheit")
    flagCToK := flag.Bool("ctok", false, "Celsius to Kelvin")
    flagFToC := flag.Bool("ftoc", false, "Fahrenheit to Celsius")
    flagFToK := flag.Bool("ftok", false, "Fahrenheit to Kelvin")
    flagKToC := flag.Bool("ktoc", false, "Kelvin to Celsius")
    flagKToF := flag.Bool("ktof", false, "Kelvin to Fahrenheit")

    // Length conversions
    flagMToCm := flag.Bool("mtocm", false, "Meters to centimeters")
    flagMToMm := flag.Bool("mtomm", false, "Meters to milimeters")
    flagMToKm := flag.Bool("mtokm", false, "Meters to kilometers")

    flagCmToM := flag.Bool("cmtom", false, "Centimeters to meters")
    flagCmToMm := flag.Bool("cmtomm", false, "Centimeters to milimeters")
    flagCmToKm := flag.Bool("cmtokm", false, "Centimeters to kilometers")

    flagMmToM := flag.Bool("mmtom", false, "Milimeters to meters")
    flagMmToCm := flag.Bool("mmtocm", false, "Milimeters to centimeters")
    flagMmToKm := flag.Bool("mmtokm", false, "Milimeters to kilometers")

    flagFtToIn := flag.Bool("fttoin", false, "Feet to inches")
    flagFtToYd := flag.Bool("fttoyd", false, "Feet to yards")
    flagInToFt := flag.Bool("intoft", false, "Inches to feet")
    flagInToYd := flag.Bool("intoyd", false, "Inches to yards")
    flagYdToIn := flag.Bool("ydtoin", false, "Yards to inches")
    flagYdToFt := flag.Bool("ydtoft", false, "Yards to feet")

    // Parse
    flag.Parse()

    // Get user input
    user_input, err := get_conversion()
    if err != nil {
	panic(err)
    }

    // Parse input to float
    user_float, err := user_input.toFloat()
    if err != nil {
	panic(err)
    }

    // Convert switch
    var r string
    switch {

    // Temperature conversions
    case *flagCToF:
	n := tempconv.Celsius(user_float)
	r = fmt.Sprintf("%v", tempconv.CToF(n))
    case * flagCToK:
	n := tempconv.Celsius(user_float)
	r = fmt.Sprintf("%v", tempconv.CToK(n))
    case *flagFToC:
	n := tempconv.Fahrenheit(user_float)
	r = fmt.Sprintf("%v", tempconv.FToC(n))
    case * flagFToK:
	n := tempconv.Fahrenheit(user_float)
	r = fmt.Sprintf("%v", tempconv.FToK(n))
    case * flagKToC:
	n := tempconv.Kelvin(user_float)
	r = fmt.Sprintf("%v", tempconv.KToC(n))
    case * flagKToF:
	n := tempconv.Kelvin(user_float)
	r = fmt.Sprintf("%v", tempconv.KToF(n))

    // Length conversions
    case *flagMToCm:
	n := lenconv.Meter(user_float)
	r = fmt.Sprintf("%v", lenconv.MToCm(n))
    case *flagMToMm:
	n := lenconv.Meter(user_float)
	r = fmt.Sprintf("%v", lenconv.MToMm(n))
    case *flagMToKm:
	n := lenconv.Meter(user_float)
	r = fmt.Sprintf("%v", lenconv.MToKm(n))
    case *flagCmToM:
	n := lenconv.Centimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.CmToM(n))
    case *flagCmToMm:
	n := lenconv.Centimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.CmToMm(n))
    case *flagCmToKm:
	n := lenconv.Centimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.CmToKm(n))
    case *flagMmToM:
	n := lenconv.Milimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.MmToM(n))
    case *flagMmToCm:
	n := lenconv.Milimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.MmToCm(n))
    case *flagMmToKm:
	n := lenconv.Milimeter(user_float)
	r = fmt.Sprintf("%v", lenconv.MmToKm(n))
    // Imperial - Imperial
    case *flagFtToIn:
	n := lenconv.Foot(user_float)
	r = fmt.Sprintf("%v", lenconv.FtToIn(n))
    case *flagFtToYd:
	n := lenconv.Foot(user_float)
	r = fmt.Sprintf("%v", lenconv.FtToYd(n))
    case *flagInToFt:
	n := lenconv.Inch(user_float)
	r = fmt.Sprintf("%v", lenconv.InToFt(n))
    case *flagInToYd:
	n := lenconv.Inch(user_float)
	r = fmt.Sprintf("%v", lenconv.InToYd(n))
    case *flagYdToIn:
	n := lenconv.Yard(user_float)
	r = fmt.Sprintf("%v", lenconv.YdToIn(n))
    case *flagYdToFt:
	n := lenconv.Yard(user_float)
	r = fmt.Sprintf("%v", lenconv.YdToFt(n))

    // Invalid
    default:
	panic("Not a valid conversion")
    }

    // Print
    fmt.Println(r)
}

//########################################
// Functions
//########################################

// Tries to get a command line 
func get_conversion() (conversion, error) {
    // Try getting from flag
    a := flag.Args()
    if len(a) == 1 {
	var from_flag conversion
	from_flag.Status = 0
	from_flag.Value  = a[0]
	return from_flag, nil
    }

    // Try getting pipe
    scanner := bufio.NewScanner(os.Stdin)
    for scanner.Scan() {
	t := scanner.Text()
	if t != "" {
	    var from_stdin conversion
	    from_stdin.Status = 0
	    from_stdin.Value  = t
	    return from_stdin, nil
	}
	if err := scanner.Err(); err != nil {
	    fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
    }

    // If neither option succeeded
    var failed conversion
    failed.Status = 1
    failed.Value  = ""
    return failed, errors.New("No data provided")
}
